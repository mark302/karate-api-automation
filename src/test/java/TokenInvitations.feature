Feature: Create Session Token for Identity Exists/New User Invitations

Background: 
* url 'http://api-dev.therecspot.com:5000/'
* header Accept = 'application/json'

Scenario: Create Token for New User Identity/Invites
Given path 'query'
Given text query = 
"""
mutation {
  requestUnverifiedSMSSessionToken(phoneNumber: "16665550003"){

   cookieKey
    tokenValue
    expires
      accountAssociated
      identity {
        id
        personas
        linkedPhoneNumber
        lastLoginAt
        createdAt
        
      }
  }
}

"""

And request { query: '#(query)' }
When method POST
Then status 200
And print response
* def linkedPhoneNumber = response.data.requestUnverifiedSMSSessionToken.identity.linkedPhoneNumber
* def sessionToken = response.data.requestUnverifiedSMSSessionToken.tokenValue
* def cookieKey = response.data.requestUnverifiedSMSSessionToken.cookieKey


