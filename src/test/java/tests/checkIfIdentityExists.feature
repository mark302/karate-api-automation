Feature: Check if Linked Phone Number already has user attached

  Background: 
    * url 'http://api-dev.therecspot.com:5000/'
    #should be mySession below not myFeature
    * def myFeature = call read('../TokenInvitations.feature')
    * def authToken = myFeature.sessionToken
    * def linkedNumber = myFeature.linkedPhoneNumber
    * configure headers = { 'X_RECSPOT_SESSION_TOKEN': '#(authToken)' , 'Content-Type': 'application/json' }

  Scenario: Check Identity Exists = True
    Given path 'query'
    And header Accept = 'application/json'
    And def phoneNumber = linkedNumber
    Given text query =
      """
      query{
      checkIdentityExistsByPhoneNumber(phoneNumber: "<phoneNumber>")
      }
      """
     And replace query.phoneNumber = phoneNumber
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And match response.data.checkIdentityExistsByPhoneNumber == true
    And print response

     Scenario: Check Identity Exists = False
    Given path 'query'
    And header Accept = 'application/json'
    And def phoneNumber = linkedNumber
    Given text query =
      """
      query{
      checkIdentityExistsByPhoneNumber(phoneNumber: "<phoneNumber>")
      }
      """
     And replace query.phoneNumber = 0001112222
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And match response.data.checkIdentityExistsByPhoneNumber == false
    And print response