package tests;

import com.github.javafaker.Faker;

public class dataGenerator {
	public static String getRandomEmail() {
		Faker faker = new Faker();
		String email = faker.name().firstName().toLowerCase() + faker.random().nextInt(0,100) + "@recspot.co";
		return email;
	}
	
	public static String getRandomUsername() {
		Faker faker = new Faker();
		String username = faker.name().username();
		return username;
	}
	
	public static String getRandomName() {
		Faker faker = new Faker();
		String name = faker.name().firstName() + " " + faker.name().firstName();
		return name;
	}
	

	
}