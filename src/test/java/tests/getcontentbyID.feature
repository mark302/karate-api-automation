Feature: Testing the Get Content by ID 

  Background: 
    * url 'http://api-dev.therecspot.com:5000/'
    #should be mySession below not myFeature
    * def myFeature = call read('../TokencreateUser.feature')
    * def authToken = myFeature.sessionToken
    * def dataGenerator = Java.type('tests.dataGenerator')
    * configure headers = { 'X_RECSPOT_SESSION_TOKEN': '#(authToken)' , 'Content-Type': 'application/json' }

  Scenario: Onboarding/Get Started
    Given path 'query'
    And header Accept = 'application/json'
    * def contentType = "MOVIE"
 
    Given text query =
      """
      query{
  getContentById(id: "99421a36-095f-4503-807f-3ad817ee5a1c", contentType:<contentType>){
  ... on Movie {
    id
    title
    description
    posterImageURL
    actors
    writers
    directors
    released
  }
  ... on TVShow {
    id
    title
    description
    posterImageURL
    actors
    writers
    released
  }
}
}
      """
    And replace query.contentType = contentType
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And print response