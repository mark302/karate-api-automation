@ignore
Feature: User Signs Up and Searches for Content

  Background: 
    * url 'http://api-dev.therecspot.com:5000/'
    #should be mySession below not myFeature
    * def myFeature = call read('../TokenSearchDiscover.feature')
    * def authToken = myFeature.sessionToken
    * def dataGenerator = Java.type('tests.dataGenerator')
    * def sleep =
      """
      function(seconds){
        for(i = 0; i <= seconds; i++)
        {
          java.lang.Thread.sleep(1*1000);
          karate.log(i);
        }
      }
      """
    * configure headers = { 'X_RECSPOT_SESSION_TOKEN': '#(authToken)' , 'Content-Type': 'application/json' }

  Scenario: User signs Up, Waits for index, and does a Search
    Given path 'query'
    And header Accept = 'application/json'
    And def email = dataGenerator.getRandomEmail()
    And def username = dataGenerator.getRandomUsername()
    And def name = dataGenerator.getRandomName()
    # Create the User
    Given text query =
      """
      mutation {
        createUser(username: "<username>", name: "<name>", email: "<email>", isPrivate: true, location: "San Francisco", birthDate: "05/05/1950") {
          avatarImageUrl
          bio
          birthDate
          createdAt
          email
          id
          isChatUser
          isDeactivated
          isPrivate
        isRecspotView
          lastLoginAt
          location
          name
          notificationPreferences{
           notificationsEnabled
          }
          pointCount
          updatedAt
          username
         
        }
      }
      """
    And replace query.username = username
    And replace query.email = email
    And replace query.name = name
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And print response
    And match response.data.createUser.id == "#uuid"
    * def userID = response.data.createUser.id
    
    
    #User has to wait to be indexed before searching
    And callonce sleep 340
    # User searches for Breaking Bad w/o filters
    Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
      {
      searchContent(userID: "<userID>"   input: {title: "Breaking Bad", pageNumber: 1}){
      totalPages
      totalElements
      pageSize
      pageNumber
      results {
      ... on MovieResults {
        result {
          typeOf: __typename
          id
          tmdbId
          title
          genres
          runtime
          posterImageURL
          backdropImageURL
          released
        }
        mostRecentFriendsLiked {
          avatarImageUrl
        }
        mostRecentFriendsDisliked {
          avatarImageUrl
        }
        friendsLikedCount
        friendsDislikedCount
      }
      ... on TVShowResults {
        result {
          typeOf: __typename
          id
          tmdbId
          title
          genres
          posterImageURL
          backdropImageURL
          released
          runtime
        }
        mostRecentFriendsLiked {
          avatarImageUrl
        }
        mostRecentFriendsDisliked {
          avatarImageUrl
        }
        friendsLikedCount
        friendsDislikedCount
      }
      }
      }
      }

      """
    And replace query.userID = userID
    And request { query: '#(query)' }
    When method POST
    Then status 200
    # assert against the response
    And print response
    And match response.data.searchContent.results[0].result.typeOf == "TVShow"
    And match response.data.searchContent.results[0].result.id == "e6907e76-0aa6-4abf-84cd-3ac59d4e88d3"
    And match response.data.searchContent.results[0].result.title == "Breaking Bad"
    And match response.data.searchContent.results[0].result.released == "2008-01-20T00:00:00Z"
    And match response.data.searchContent.results[0].result.tmdbId == "1396"
    And match response.data.searchContent.results[0].result.genres[0] !=null
    And match response.data.searchContent.results[0].result.posterImageURL == "/ggFHVNu6YYI5L9pCfOacjizRGt.jpg"
    And match response.data.searchContent.results[0].result.released == "2008-01-20T00:00:00Z"
    And match response.data.searchContent.results[0].result.runtime == "45m0s"
    #And match response.data.searchContent.results[0].result.backdropImageURL == "/tsRy63Mu5cu8etL1X7ZLyf7UP1M.jpg"
    # User searches for Star Wars w/o filters
    Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
      {
      searchContent(userID: "<userID>"   input: {title: "Star Wars", pageNumber: 1}){
      totalPages
      totalElements
      pageSize
      pageNumber
      results {
      ... on MovieResults {
        result {
          typeOf: __typename
          id
          tmdbId
          title
          genres
          runtime
          posterImageURL
          backdropImageURL
          released
        }
        mostRecentFriendsLiked {
          avatarImageUrl
        }
        mostRecentFriendsDisliked {
          avatarImageUrl
        }
        friendsLikedCount
        friendsDislikedCount
      }
      ... on TVShowResults {
        result {
          typeOf: __typename
          id
          tmdbId
          title
          genres
          posterImageURL
          backdropImageURL
          released
          runtime
        }
        mostRecentFriendsLiked {
          avatarImageUrl
        }
        mostRecentFriendsDisliked {
          avatarImageUrl
        }
        friendsLikedCount
        friendsDislikedCount
      }
      }
      }
      }

      """
    And replace query.userID = userID
    And request { query: '#(query)' }
    When method POST
    Then status 200
    # assert against the response
    And print response
    And match response.data.searchContent.results[0].result.typeOf == "Movie"
    And match response.data.searchContent.results[0].result.id == "ee2aaa8a-d753-4857-a263-7e1a93effbd1"
    And match response.data.searchContent.results[0].result.tmdbId == "11"
    And match response.data.searchContent.results[0].result.title == "Star Wars"
    And match response.data.searchContent.results[0].result.genres[0] !=null
    And match response.data.searchContent.results[0].result.runtime == "2h1m0s"
    And match response.data.searchContent.results[0].result.posterImageURL == "/6FfCtAuVAW8XJjZ7eWeLibRLWTw.jpg"
    And match response.data.searchContent.results[0].result.released == "1977-05-25T00:00:00Z"
    #And match response.data.searchContent.results[0].mostRecentFriendsLiked
    #And match response.data.searchContent.results[0].mostRecentFriendsDisliked
    #And match response.data.searchContent.results[0].friendsLikedCount
    #And match response.data.searchContent.results[0].friendsLikedCount
    # Movie with Filter
    Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
      {
      searchContent(userID: "<userID>"   input: {title: "The Big Leboswki", pageNumber: 1, mediaType: MOVIE}){
      totalPages
      totalElements
      pageSize
      pageNumber
      results {
      ... on MovieResults {
        result {
          typeOf: __typename
          id
          tmdbId
          title
          genres
          runtime
          posterImageURL
          backdropImageURL
          released
        }
        mostRecentFriendsLiked {
          avatarImageUrl
        }
        mostRecentFriendsDisliked {
          avatarImageUrl
        }
        friendsLikedCount
        friendsDislikedCount
      }
      ... on TVShowResults {
        result {
          typeOf: __typename
          id
          tmdbId
          title
          genres
          posterImageURL
          backdropImageURL
          released
          runtime
        }
        mostRecentFriendsLiked {
          avatarImageUrl
        }
        mostRecentFriendsDisliked {
          avatarImageUrl
        }
        friendsLikedCount
        friendsDislikedCount
      }
      }
      }
      }

      """
    And replace query.userID = userID
    And request { query: '#(query)' }
    When method POST
    Then status 200
    # assert against the response
    And print response
    And match response.data.searchContent.results[0].result.typeOf == "Movie"
    And match response.data.searchContent.results[0].result.id == "3b45fff9-60e6-4aa6-8be8-e9da5e9a2336"
    And match response.data.searchContent.results[0].result.tmdbId == "115"
    And match response.data.searchContent.results[0].result.title == "The Big Lebowski"
    And match response.data.searchContent.results[0].result.genres[0] !=null
    And match response.data.searchContent.results[0].result.runtime == "1h57m0s"
    And match response.data.searchContent.results[0].result.posterImageURL == "/48auuGpzl9KSZixAKwj8yKBNIv4.jpg"
    And match response.data.searchContent.results[0].result.released == "1998-03-06T00:00:00Z"
    #And match response.data.searchContent.results[0].mostRecentFriendsLiked
    #And match response.data.searchContent.results[0].mostRecentFriendsDisliked
    #And match response.data.searchContent.results[0].friendsLikedCount
    #And match response.data.searchContent.results[0].friendsLikedCount
    # Using Filter = Series
    Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
      {
      searchContent(userID: "<userID>"   input: {title: "The Sopranos", pageNumber: 1, mediaType: SERIES}){
      totalPages
      totalElements
      pageSize
      pageNumber
      results {
      ... on MovieResults {
        result {
          typeOf: __typename
          id
          tmdbId
          title
          genres
          runtime
          posterImageURL
          backdropImageURL
          released
        }
        mostRecentFriendsLiked {
          avatarImageUrl
        }
        mostRecentFriendsDisliked {
          avatarImageUrl
        }
        friendsLikedCount
        friendsDislikedCount
      }
      ... on TVShowResults {
        result {
          typeOf: __typename
          id
          tmdbId
          title
          genres
          posterImageURL
          backdropImageURL
          released
          runtime
        }
        mostRecentFriendsLiked {
          avatarImageUrl
        }
        mostRecentFriendsDisliked {
          avatarImageUrl
        }
        friendsLikedCount
        friendsDislikedCount
      }
      }
      }
      }

      """
    And replace query.userID = userID
    And request { query: '#(query)' }
    When method POST
    Then status 200
    # assert against the response
    And print response
    And match response.data.searchContent.results[0].result.typeOf == "TVShow"
    And match response.data.searchContent.results[0].result.id == "36cfca57-6763-4c7d-ba1f-3679494fe709"
    And match response.data.searchContent.results[0].result.tmdbId == "1398"
    And match response.data.searchContent.results[0].result.title == "The Sopranos"
    And match response.data.searchContent.results[0].result.genres[0] !=null
    And match response.data.searchContent.results[0].result.runtime == "1h0m0s"
    And match response.data.searchContent.results[0].result.posterImageURL == "/57okJJUBK0AaijxLh3RjNUaMvFI.jpg"
    And match response.data.searchContent.results[0].result.released == "1999-01-10T00:00:00Z"
