
Feature: healthcheck

Background: 
* url 'http://api-dev.therecspot.com:5000/'
* def myFeature = call read('../Tokenversionhealth.feature')
* def authToken = myFeature.sessionToken
* configure headers = { 'X_RECSPOT_SESSION_TOKEN': '#(authToken)' , 'Content-Type': 'application/json' }

Scenario: get server healthcheck
Given path 'query'
And header Accept = 'application/json'
Given text query = 
"""
mutation {
healthcheck
}

"""

And request { query: '#(query)' }
When method POST
Then status 200
And print response