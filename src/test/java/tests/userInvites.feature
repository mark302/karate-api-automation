@ignore
Feature: User Signs Up and Invites via Email and Phone

  Background: 
    * url 'http://api-dev.therecspot.com:5000/'
    #should be mySession below not myFeature
    * def myFeature = call read('../TokenInvitations.feature')
    * def authToken = myFeature.sessionToken
    * def dataGenerator = Java.type('tests.dataGenerator')
    * configure headers = { 'X_RECSPOT_SESSION_TOKEN': '#(authToken)' , 'Content-Type': 'application/json' }

  Scenario: New User created and invites via email/phone
    Given path 'query'
    And header Accept = 'application/json'
    And def email = dataGenerator.getRandomEmail()
    And def username = dataGenerator.getRandomUsername()
    And def name = dataGenerator.getRandomName()
    # Create the User and invite
    Given text query =
      """
      mutation {
        createUser(username: "<username>", name: "<name>", email: "<email>", isPrivate: true, location: "San Francisco", birthDate: "05/05/1950") {
          avatarImageUrl
          bio
          birthDate
          createdAt
          email
          id
          isChatUser
          isDeactivated
          isPrivate
        isRecspotView
          lastLoginAt
          location
          name
          notificationPreferences{
           notificationsEnabled
          }
          pointCount
          updatedAt
          username
         
        }
      }
      """
    And replace query.username = username
    And replace query.email = email
    And replace query.name = name
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And print response
    And match response.data.createUser.id == "#uuid"
    And match response.data.createUser.username !=null
    And match response.data.createUser.lastLoginAt !=null
    And match response.data.createUser.createdAt !=null
    * def avatarImageUrl = response.data.createUser.avatarImageUrl
    * def bio = response.data.createUser.bio
    * def birthDate = response.data.createUser.birthDate
    * def createdAt = response.data.createUser.createdAt
    * def userID = response.data.createUser.id
    * def email = response.data.createUser.email
    * def username = response.data.createUser.username
    * def name = response.data.createUser.name
    
   Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
      mutation {
      inviteViaPhoneNumbers (userID: "<userID>", phoneNumbers: ["16505323475", "14152363969"])}

      """
    And replace query.userID = userID
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And match response.data.inviteViaPhoneNumbers == true
    And print response
    # New user invites via email addresses
    Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
      mutation {
      inviteViaEmails (userID: "<userID>", emails: ["spam@therecspot.co", "spam@therecspot.com"])}

      """
    And replace query.userID = userID
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And match response.data.inviteViaEmails == true
    And print response