
Feature: User Signs Up and Navigates to Search/Discover

  Background: 
    * url 'http://api-dev.therecspot.com:5000/'
    #should be mySession below not myFeature
    * def myFeature = call read('../TokenSearchDiscover.feature')
    * def authToken = myFeature.sessionToken
    * def dataGenerator = Java.type('tests.dataGenerator')
    * def sleep =
      """
      function(seconds){
        for(i = 0; i <= seconds; i++)
        {
          java.lang.Thread.sleep(1*1000);
          karate.log(i);
        }
      }
      """
   
      #* def uppercase =
      #"""
      #
    * configure headers = { 'X_RECSPOT_SESSION_TOKEN': '#(authToken)' , 'Content-Type': 'application/json' }

  Scenario: User signs Up and visits Discover/Search
    Given path 'query'
    And header Accept = 'application/json'
    And def email = dataGenerator.getRandomEmail()
    And def username = dataGenerator.getRandomUsername()
    And def name = dataGenerator.getRandomName()
    # Create the User
    Given text query =
      """
      mutation {
        createUser(username: "<username>", name: "<name>", isPrivate: true, location: "San Francisco", birthDate: "05/05/1950") {
          avatarImageUrl
          bio
          birthDate
          createdAt
          email
          id
          isChatUser
          isDeactivated
          isPrivate
        isRecspotView
          lastLoginAt
          location
          name
          notificationPreferences{
           notificationsEnabled
          }
          pointCount
          updatedAt
          username
         
        }
      }
      """
    And replace query.username = username
    #And replace query.email = email
    And replace query.name = name
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And print response
    And match response.data.createUser.id == "#uuid"
    * def userID = response.data.createUser.id
    And callonce sleep 5
    #User gets list of Public Lists
    Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
      query {
      getPublicLists {
      id
      title
      categories
      isVisible
      screenTags
      items {
      ... on Movie {
        id
        tmdbId
        title
        genres
        runtime
        released
        posterImageURL
        backdropImageURL
      }
      ... on TVShow {
        id
        tmdbId
        title
        runtime
        genres
        released
        posterImageURL
        backdropImageURL
      }
      }
      createdAt
      updatedAt
      }
      }

      """
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And print response
    # define the stuff needed for pulling lists
    * def listID = response.data.getPublicLists[0].id
    # assert against the response
    And match response.data.getPublicLists[0].id !=null
    And match response.data.getPublicLists[0].title !=null
    And match response.data.getPublicLists[0].isVisible == true
    And match response.data.getPublicLists[0].items[0].tmdbId !=null
    And match response.data.getPublicLists[0].items[0].title !=null
    And match response.data.getPublicLists[0].items[0].genres[0] !=null
    And match response.data.getPublicLists[0].items[0].runtime !=null
    And match response.data.getPublicLists[0].items[0].released !=null
    And match response.data.getPublicLists[0].items[0].posterImageURL !=null
    And match response.data.getPublicLists[0].items[0].backdropImageURL !=null
    # user clicks one of the public lists
    Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
      query{
      getPublicList(listID:"<listID>")
      {
      id
      title
      categories
      isVisible
      screenTags
      items {
      ... on Movie {
      __typename
        id
        tmdbId
        title
        genres
        released
        posterImageURL
        backdropImageURL
      }
      ... on TVShow {
      __typename
        id
        tmdbId
        title
        genres
        released
        posterImageURL
        backdropImageURL
      }
      }
      createdAt
      updatedAt
      }
      }
      """
    And replace query.listID = listID
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And print response
    * def itemID1 = response.data.getPublicList.items[3].id
    * def contentType1LC = response.data.getPublicList.items[0].__typename
    * def contentType1 = contentType1LC.toUpperCase()
    * def contentType1 = contentType1 == 'TVSHOW' ? 'SERIES' : contentType1


    * def itemID2 = response.data.getPublicList.items[7].id
    * def contentType2LC = response.data.getPublicList.items[7].__typename
    * def contentType2 = contentType2LC.toUpperCase()
    * def contentType2 = contentType2 == 'TVSHOW' ? 'SERIES' : contentType2
    
    * def itemID3 = response.data.getPublicList.items[15].id
    * def contentType3LC = response.data.getPublicList.items[15].__typename
    * def contentType3 = contentType3LC.toUpperCase()
    * def contentType3 = contentType3 == 'TVSHOW' ? 'SERIES' : contentType3
 
    
    * def itemID4 = response.data.getPublicList.items[30].id
    * def contentType4LC = response.data.getPublicList.items[30].__typename
    * def contentType4 = contentType4LC.toUpperCase()
    * def contentType4 = contentType4 == 'TVSHOW' ? 'SERIES' : contentType4
    # assert on the contents of the list
    And match response.data.getPublicList.items[2].id !=null
    And match response.data.getPublicList.items[2].tmdbId !=null
    And match response.data.getPublicList.items[2].title !=null
    And match response.data.getPublicList.items[2].genres[0] !=null
    And match response.data.getPublicList.items[2].released !=null
    And match response.data.getPublicList.items[2].posterImageURL !=null
    And match response.data.getPublicList.items[2].backdropImageURL !=null
    And match response.data.getPublicList.items[6].id !=null
    And match response.data.getPublicList.items[6].tmdbId !=null
    And match response.data.getPublicList.items[6].title !=null
    And match response.data.getPublicList.items[6].genres[0] !=null
    And match response.data.getPublicList.items[6].released !=null
    And match response.data.getPublicList.items[6].posterImageURL !=null
    And match response.data.getPublicList.items[6].backdropImageURL !=null
    And match response.data.getPublicList.items[14].id !=null
    And match response.data.getPublicList.items[14].tmdbId !=null
    And match response.data.getPublicList.items[14].title !=null
    And match response.data.getPublicList.items[14].genres[0] !=null
    And match response.data.getPublicList.items[14].released !=null
    And match response.data.getPublicList.items[14].posterImageURL !=null
    And match response.data.getPublicList.items[14].backdropImageURL !=null
    And match response.data.getPublicList.items[29].id !=null
    And match response.data.getPublicList.items[29].tmdbId !=null
    And match response.data.getPublicList.items[29].title !=null
    And match response.data.getPublicList.items[29].genres[0] !=null
    And match response.data.getPublicList.items[29].released !=null
    And match response.data.getPublicList.items[29].posterImageURL !=null
    And match response.data.getPublicList.items[29].backdropImageURL !=null
    #
    # User clicks on content details for items in public list
     Given path 'query'
    And header Accept = 'application/json'
    Given text query =
      """
    query {
  getContentById(id: "<itemID1>", contentType: <contentType1>) {

    ... on Movie{
    id
    title
    tmdbId
    genres
    rated
    released
    runtime
    plot
    languages
    countries
    awards
    description
    posterImageURL
    backdropImageURL
    actors
    writers
    directors   
    }
    
    ... on TVShow{
    id
    tmdbId
    title
    genres
    rated
    released
    runtime
    plot
    languages
    countries
    awards
    description
    posterImageURL
    backdropImageURL
    actors
    writers
    totalSeasons   
    }
  }
  }

      """
    And replace query.itemID1 = itemID1
    And replace query.contentType1 = contentType1
    And request { query: '#(query)' }
    When method POST
    Then status 200
    And print response
#assert on the content details


